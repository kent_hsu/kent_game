exports = typeof window !== "undefined" && window !== null ? window : global;

exports.Game = function() {
  var players          = new Array();
  var playerCurrentPlaces    = new Array(6);
  var purses           = new Array(6);
  var inPenaltyBox     = new Array(6);

  var popQuestions     = new Array();
  var scienceQuestions = new Array();
  var sportsQuestions  = new Array();
  var rockQuestions    = new Array();

  var currentPlayer    = 0;
  var isGettingOutOfPenaltyBox = false;

  var isPlayerWin = function(){
    return !(purses[currentPlayer] == 6)
  };

  var getCurrentPlaceQuestionCategory = function(){
    if(playerCurrentPlaces[currentPlayer] == 0)
      return 'Pop';
    if(playerCurrentPlaces[currentPlayer] == 4)
      return 'Pop';
    if(playerCurrentPlaces[currentPlayer] == 8)
      return 'Pop';
    if(playerCurrentPlaces[currentPlayer] == 1)
      return 'Science';
    if(playerCurrentPlaces[currentPlayer] == 5)
      return 'Science';
    if(playerCurrentPlaces[currentPlayer] == 9)
      return 'Science';
    if(playerCurrentPlaces[currentPlayer] == 2)
      return 'Sports';
    if(playerCurrentPlaces[currentPlayer] == 6)
      return 'Sports';
    if(playerCurrentPlaces[currentPlayer] == 10)
      return 'Sports';
    return 'Rock';
  };

  this.createRockQuestion = function(index){
    return "Rock Question "+index;
  };

  for(var i = 0; i < 50; i++){
    popQuestions.push("Pop Question "+i);
    scienceQuestions.push("Science Question "+i);
    sportsQuestions.push("Sports Question "+i);
    rockQuestions.push(this.createRockQuestion(i));
  };

  this.isPlayable = function(howManyPlayers){
    return howManyPlayers >= 2;
  };

  this.add = function(playerName){
    players.push(playerName);
    playerCurrentPlaces[this.howManyPlayers() - 1] = 0;
    purses[this.howManyPlayers() - 1] = 0;
    inPenaltyBox[this.howManyPlayers() - 1] = false;

    console.log(playerName + " was added");
    console.log("They are player number " + players.length);

    return true;
  };

  this.howManyPlayers = function(){
    return players.length;
  };


  var askQuestion = function(){
    if(getCurrentPlaceQuestionCategory() == 'Pop')
      console.log(popQuestions.shift());
    if(getCurrentPlaceQuestionCategory() == 'Science')
      console.log(scienceQuestions.shift());
    if(getCurrentPlaceQuestionCategory() == 'Sports')
      console.log(sportsQuestions.shift());
    if(getCurrentPlaceQuestionCategory() == 'Rock')
      console.log(rockQuestions.shift());
  };

  this.roll = function(roll){
    console.log(players[currentPlayer] + " is the current player");
    console.log("They have rolled a " + roll);

    if(inPenaltyBox[currentPlayer]){
      if(roll % 2 != 0){
        isGettingOutOfPenaltyBox = true;

        console.log(players[currentPlayer] + " is getting out of the penalty box");
        playerCurrentPlaces[currentPlayer] = playerCurrentPlaces[currentPlayer] + roll;
        if(playerCurrentPlaces[currentPlayer] > 11){
          playerCurrentPlaces[currentPlayer] = playerCurrentPlaces[currentPlayer] - 12;
        }

        console.log(players[currentPlayer] + "'s new location is " + playerCurrentPlaces[currentPlayer]);
        console.log("The category is " + getCurrentPlaceQuestionCategory());
        askQuestion();
      }else{
        console.log(players[currentPlayer] + " is not getting out of the penalty box");
        isGettingOutOfPenaltyBox = false;
      }
    }else{

      playerCurrentPlaces[currentPlayer] = playerCurrentPlaces[currentPlayer] + roll;
      if(playerCurrentPlaces[currentPlayer] > 11){
        playerCurrentPlaces[currentPlayer] = playerCurrentPlaces[currentPlayer] - 12;
      }

      console.log(players[currentPlayer] + "'s new location is " + playerCurrentPlaces[currentPlayer]);
      console.log("The category is " + getCurrentPlaceQuestionCategory());
      askQuestion();
    }
  };

  this.wasCorrectlyAnswered = function(){
    if(inPenaltyBox[currentPlayer]){
      if(isGettingOutOfPenaltyBox){
        console.log('Answer was correct!!!!');
        purses[currentPlayer] += 1;
        console.log(players[currentPlayer] + " now has " +
                    purses[currentPlayer]  + " Gold Coins.");

        var winner = isPlayerWin();
        currentPlayer += 1;
        if(currentPlayer == players.length)
          currentPlayer = 0;

        return winner;
      }else{
        currentPlayer += 1;
        if(currentPlayer == players.length)
          currentPlayer = 0;
        return true;
      }



    }else{

      console.log("Answer was correct!!!!");

      purses[currentPlayer] += 1;
      console.log(players[currentPlayer] + " now has " +
                  purses[currentPlayer]  + " Gold Coins.");

      var winner = isPlayerWin();

      currentPlayer += 1;
      if(currentPlayer == players.length)
        currentPlayer = 0;

      return winner;
    }
  };

  this.wrongAnswer = function(){
		console.log('Question was incorrectly answered');
		console.log(players[currentPlayer] + " was sent to the penalty box");
		inPenaltyBox[currentPlayer] = true;

    currentPlayer += 1;
    if(currentPlayer == players.length)
      currentPlayer = 0;
		return true;
  };
};

var notAWinner = false;

var game = new Game();

game.add('Chet');
game.add('Pat');
game.add('Sue');

do{

  game.roll(Math.floor(Math.random()*6) + 1);

  if(Math.floor(Math.random()*10) == 7){
    notAWinner = game.wrongAnswer();
  }else{
    notAWinner = game.wasCorrectlyAnswered();
  }

}while(notAWinner);
